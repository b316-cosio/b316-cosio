package com.zuitt.example;

public class Car {
    //Properties/Attributes - the characteristics of the object the class will create
    //Constructor - method to create the object and instantiate with its initialized value
    //Getters and setters - are methods to get values of an object properties  or set them
    //Methods - actions that an object can perform or do.


    //Public access - the variable/property in the class is accessible anywhere in the application
    //Private - limits the access and ability to get or set a variable/method to only within its own class
    // Getters - methods that return the value of the property
    //Setters - methods that allow us to set the value of a property
    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    //Constructor is a method which allows us to set the initial value of an instance
    public Car() {
        this.carDriver = new Driver();
    }

    public Car(String make, String brand, int price, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    //Getter and Setter for our properties
    //Getters return a value so, therefore we must add the data type of the value returned.
    public String getMake(){
        //"this" keyword refers to the object/isntance where the constructor or setter/getter is
        return this.make;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }

    //Mini-Activity
    //Create getters and setters for the brand and price properties
    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Driver getCarDriver(){
        return carDriver;
    }

    public void setCarDriver(Driver carDriver){
        this.carDriver = carDriver;
    }

    //Custom method to retrieve the car driver's name
    public String getCarDriverName(){
        return this.carDriver.getName();
    }

    //Methods are functions o
    public void start(){
        System.out.println("Vroom Vroom!");
    }

}
