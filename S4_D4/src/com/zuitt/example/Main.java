package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
       Car car2 = new Car();
//        car2.make = "GTR Skyline";
//        car2.brand = "Nissan";
//        car2.price = 120990;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
        Car car3 = new Car();
//        car3.make = "Supra";
//        car3.brand = "Toyota";
//        car3.price = 57750;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);

Driver driver1 = new Driver("Alejandro", 25);
//System.out.println(driver1.name);

        car1.start();
        car2.start();
        car3.start();

        //Property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        //Property Setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("GTR Skyline");
        System.out.println(car2.getMake());

        //carDriver Getter
        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        // Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

    }
}


