package com.zuitt.activity1;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {

        Scanner userInfo = new Scanner(System.in);

        System.out.print("Enter your first name: ");
        String firstName = userInfo.nextLine();

        System.out.print("Enter your last name: ");
        String lastName = userInfo.nextLine();

        System.out.print("Enter your grade for the first subject: ");
        double firstSubject = userInfo.nextDouble();

        System.out.print("Enter your grade for the second subject: ");
        double secondSubject = userInfo.nextDouble();

        System.out.print("Enter your grade for the third subject: ");
        double thirdSubject = userInfo.nextDouble();

        double averageGrade = Math.round((firstSubject + secondSubject + thirdSubject) / 3);

        System.out.println("Good Day, : " + firstName + " " + lastName);
        System.out.println("Your Average Grade is: " + averageGrade);
    }
}
