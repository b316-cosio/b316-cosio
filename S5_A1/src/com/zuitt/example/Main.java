package com.zuitt.example;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", new String[]{"+639152468596", "+639228547963"},
                new String[]{"Quezon City", "Makati City"});
        Contact contact2 = new Contact("Jane Doe", new String[]{"+639162148573", "+639173698541"},
                new String[]{"Caloocan City", "Pasay City"});

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("-----------------------------------------------");

                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String number : contact.getContactNumber()) {
                    System.out.println(number);
                }

                System.out.println("-----------------------------------------------");

                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println("My home address in " + contact.getAddress()[0]);
                System.out.println("My office address in " + contact.getAddress()[1]);
                System.out.println("===============================================");
            }
        }

        User user = new User("Terrence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        Course course = new Course();

        course.setName("MACQ004");
        course.setDescription("This Course can be described as an introduction to Java for career shifters.");
        course.setSeats(30);
        course.setFee(2500.0);
        course.setStartDate(new Date());
        course.setEndDate(new Date());
        course.setInstructor(user);

        System.out.println("Hi! I'm " + user.getName() + ". I'm " + user.getAge() + " years old. You can reach me via my email: " + user.getEmail() + ". When I'm off work, I can be found at my house in " + user.getAddress());
        System.out.println("Welcome to the course " + course.getName() + "! " + course.getDescription() + " Your instructor for this course is Sir " + course.getInstructor().getName() + ". Enjoy!");
    }
}
