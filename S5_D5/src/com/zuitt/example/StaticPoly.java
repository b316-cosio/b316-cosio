package com.zuitt.example;

public class StaticPoly {

    //Static polymorphism - This is the ability to have multiple methods of the same name but changes forms based on the number of arguments or the types of arguments

    public int addition(int a, int b){
        return a+b;
    }

    //Overload by changing the number of arguments
    public int addition(int a, int b, int c){
        return a+b+c;
    }

    //Overload by changing the type of arguments.
    public double addition(double a, double b){
        return a+b;
    }
}
