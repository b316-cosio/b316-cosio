package com.zuitt.example;

public class Main {

    public static void main(String[] args) {
        System.out.println("hello World");

        Person person1 = new Person();

        person1.run();
        person1.sleep();
        person1.noblePhantasm();
        person1.jitterAim();

        StaticPoly staticPoly = new StaticPoly();

        System.out.println(staticPoly.addition(1,5 ));
        System.out.println(staticPoly.addition(1,5, 10 ));
        System.out.println(staticPoly.addition(15.5,15.6 ));

        Parent parent1 = new Parent("John", 35);
        parent1.greet();
        parent1.greet("John", "Morning" );
        parent1.speak();

        Child newChild = new Child();
        newChild.speak();
    }
    /*
    * Mini Activity
    * Create two new actions that a person would do
    * and Implement then in the action class.
    * run them in the Main Class
    *
    *
    * */
}
