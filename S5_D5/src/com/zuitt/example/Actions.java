package com.zuitt.example;

public interface Actions {
    //Interfaces are used to achieve abstraction. It hides away the actual implementations of the methods and simply shows a "list"/ a "menu" of methods that can be done
    //Interfaces are like blueprints for your classes. Because any class that implements out interface MUST have methods in the interface
    public void sleep();
    public void run();

    public void noblePhantasm();

    public void jitterAim();
}
