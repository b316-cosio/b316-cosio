package com.zuitt.example;

public class Person implements Actions {
    //For a class to implement or use an interface, we use the implements keyword

    public void sleep(){
        System.out.println("Zzzzzzzzzzzzzz");

    }

    public void run(){
        System.out.println("Sheeeeeeeeeeeeeeeeeeeesh");

    }

    public void noblePhantasm(){
        System.out.println("Enuma elish!");

    }

    public void jitterAim(){
        System.out.println("Roller");

    }
}
