package com.zuitt.example;

import java.util.Date;

public class Main {
    public static void main(String[] args) {

        User user = new User("Terrence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        Course course = new Course();

        course.setName("MACQ004");
        course.setDescription("This Course can be described as an introduction to Java for career shifters.");
        course.setSeats(30);
        course.setFee(2500.0);
        course.setStartDate(new Date());
        course.setEndDate(new Date());
        course.setInstructor(user);


        System.out.println("Hi! I'm " + user.getName() + ". I'm " + user.getAge() + " You can reach me via my email: " + user.getEmail() + "  When I'm off work, I can be found at my house in " + user.getAddress());
        System.out.println("Welcome to the course " + course.getName() + " " + course.getDescription() + "Your instructor for this course is Sir " + course.getInstructor().getName() + ". Enjoy!");

    }
}
