package com.zuitt.example;
import java.util.Scanner;

public class S3_A1 {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);
          int num = 0;

        try {
                num = in.nextInt();
            } catch (Exception e) {
                System.out.println("Invalid input");
                e.printStackTrace();
            }

        if (num < 0) {
                System.out.println("undefined");
            } else if (num == 0) {
                System.out.println("Factorial of 0 is 1.");
            }

        int answer = 1;
        int counter = 1;

            while (counter <= num) {
             answer *= counter;
                counter++;
          }

        System.out.println("Factorial of " + num + " using a while loop: " + answer);

        answer = 1;

            for (int i = 1; i <= num; i++) {
              answer *= i;
         }

        System.out.println("Factorial of " + num + " using a for loop: " + answer);
    }
}