package com.zuitt.example;
import java.util.Scanner;

public class LeapYearInput {
    public static void isLeapYear(int year) {
        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is not a leap year.");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a year: ");
        if (scanner.hasNextInt()) {
            int year = scanner.nextInt();
            isLeapYear(year);
        } else {
            System.out.println("Invalid input. Please enter a numerical year.");
        }
    }
}
