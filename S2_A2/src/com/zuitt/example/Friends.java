package com.zuitt.example;
import java.util.ArrayList;

public class Friends {
    public static void main(String[] args) {
        ArrayList<String> friendsList = new ArrayList<>();

        friendsList.add("John");
        friendsList.add("Jane");
        friendsList.add("Chloe");
        friendsList.add("Zoey");

        System.out.println("My friends are: " + friendsList);
    }
}
