package com.zuitt.example;
import java.util.HashMap;
import java.util.ArrayList;

public class S2_A2 { public static void main(String[] args) {
    int[] primeArray = {2, 3, 5, 7, 11};

    System.out.println("The first prime number is: " + primeArray[0]);
    System.out.println("The second prime number is: " + primeArray[1]);
    System.out.println("The third prime number is: " + primeArray[2]);
    System.out.println("The fourth prime number is: " + primeArray[3]);
    System.out.println("The fifth prime number is: " + primeArray[4]);

 }
}
