package com.zuitt.example;
import java.util.HashMap;

public class InventoryList {
    public static void main(String[] args) {

        HashMap<String, Integer> products = new HashMap<>();

        products.put("toothpaste", 5);
        products.put("toothbrush", 20);
        products.put("Soap", 12);

        System.out.println("Our Current Inventory consists of: " + products);
    }
}
